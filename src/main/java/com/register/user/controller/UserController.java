package com.register.user.controller;

import com.register.user.controller.dto.UserDto;
import com.register.user.document.User;
import com.register.user.service.UserService;
import org.springframework.web.bind.annotation.*;



import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UserController
{

    private final UserService userService;

    public UserController(UserService userService)
    {
        this.userService = userService;

    }
    @PostMapping
    public User create(@RequestBody UserDto userDto)
    {
        return userService.create(new User (userDto));
    }

    @GetMapping("/all")
    public List<User> getUsersList()
    {
        return userService.all();
    }
    @GetMapping( "/{id}" )
    public User findByDni( @PathVariable String dni )
    {
        return userService.findByDni( dni );
    }

    @PutMapping("/{id}")
   public User update(@RequestParam String id, @RequestBody UserDto userDto){
            return userService.update(userDto);
        }
    @DeleteMapping("/{id}")
     public boolean delete(@RequestParam String id){
            return userService.delete( id );
      }

}
