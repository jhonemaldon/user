package com.register.user.controller.dto;

public class UserDto
{

    private String dni;

    private  String name;

    private  String lastname;

    private  String email;

    private  String phone;

    private  String password;

    public  UserDto(String dni, String name, String lastname, String email, String phone, String password)
    {
        this.dni = dni;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public UserDto() {
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

