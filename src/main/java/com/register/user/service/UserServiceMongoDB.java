package com.register.user.service;

import com.register.user.controller.dto.UserDto;
import com.register.user.document.User;
import com.register.user.exception.UserNotFoundException;
import com.register.user.respository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceMongoDB implements UserService
{
    private UserRepository userRepository;

    public UserServiceMongoDB(@Autowired  UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(User user) {
        return userRepository.save( user);
    }

    @Override
    public User findByDni( String dni) {
        return userRepository.findByDni(dni);
    }

    @Override
    public User update(UserDto userDto) {
        return null;
    }

    @Override
    public boolean delete(String dni) {
        return false;
    }

    @Override
    public List<User> all() {
        return null;
    }

    @Override
    public User findByEmail(String email) throws UserNotFoundException
    {
        Optional<User> optionalUser = userRepository.findByEmail( email );
        if ( optionalUser.isPresent() )
        {
            return optionalUser.get();
        }
        else
        {
            throw new UserNotFoundException();
        }
    }
}
