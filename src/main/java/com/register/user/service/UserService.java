package com.register.user.service;

import com.register.user.controller.dto.UserDto;
import com.register.user.document.User;
import com.register.user.exception.UserNotFoundException;

import java.util.List;

public interface UserService {

    User create(User user);

    User findByDni (String dni);


    User update(UserDto userDto);

    boolean delete (String dni);

    List<User> all();

    User findByEmail(String email);

}
