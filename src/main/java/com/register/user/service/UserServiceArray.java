package com.register.user.service;
import com.register.user.controller.dto.UserDto;
import com.register.user.document.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class UserServiceArray implements UserService
{
    private ArrayList<User> usersList = new ArrayList<>();

    @Override
    public User create(User user)
    {
        long id = usersList.size()+1;
        usersList.add(user);
        return user;
    }

    @Override
    public User findByDni(String dni)
    {
        for (User user : usersList)
        {
            if (Objects.equals(user.getDni(), dni))
            {
                return user;
            }
        }

        return null;
    }

    @Override
    public User update(UserDto userDto)
    {


        return null;
    }

    @Override
    public boolean delete(String dni)
    {
        for(User user: usersList){
            if(user.getDni().equals(dni)){
                usersList.remove(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<User> all() {
        return usersList;
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }
}
