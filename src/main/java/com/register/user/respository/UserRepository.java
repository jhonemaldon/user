package com.register.user.respository;

import com.register.user.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository  extends MongoRepository<User, String>
{
  // User findUserByEm(String email);

    User findByDni(String dni);


    Optional<User> findByEmail(String email);
}
