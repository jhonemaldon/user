package com.register.user.document;

import com.register.user.controller.dto.UserDto;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Document
public class User
{

    @Id
    private String id;

    private String dni;

    private String name;

    private String lastname;

    private String email;

    private final String phone;

    private String passwordHash;

    List<RoleEnum> roles;

    private final Date created;

    public User ( String dni, String name, String lastname, String email, String phone, String passwordHash)
    {

       this.dni =dni;
       this.name = name;
       this.lastname = lastname;
       this.email = email;
       this.phone = phone;
       this.passwordHash = passwordHash;
       this.created = new Date();
    }



    public User(UserDto userDto)
    {
        this(userDto.getDni(), userDto.getName(),userDto.getLastname(),userDto.getEmail(), userDto.getPhone(), userDto.getPassword());
        roles = new ArrayList<>( Collections.singleton( RoleEnum.USER ) );
        passwordHash = BCrypt.hashpw( userDto.getPassword(), BCrypt.gensalt() );
    }

    public Date getCreated() {return created;}

    public String getId() {return id;}

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public List<RoleEnum> getRoles()
    {
        return roles;
    }


    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void update( UserDto userDto )
    {
        this.name = userDto.getName();
        this.lastname = userDto.getLastname();
        this.email = userDto.getEmail();
        if ( userDto.getPassword() != null )
        {
            this.passwordHash = BCrypt.hashpw( userDto.getPassword(), BCrypt.gensalt() );
        }
    }
}

